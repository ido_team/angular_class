import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { User } from '../user/user';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'jce-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

user:User = {name:'', email:''};
@Output() userAddedEvent = new EventEmitter<User>();

onSubmit(form:NgForm){
  //console.log(form.form.value);
  this.userAddedEvent.emit(this.user);
  this.user = {name: '', email: ''};
}
<<<<<<< HEAD
=======

>>>>>>> 88c0e6b5d7baee815ab72670d0bd28dbd8b32b9e
  constructor() { }

  ngOnInit() {
  }

}
