import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';


@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})

export class PostsComponent implements OnInit {

posts;
isLoading:boolean = true;

showPost(){
  console.log(this)
}
  constructor(private _postsService: PostsService) { }
  ngOnInit() {
     this._postsService.getPosts().subscribe(postsData =>
    {
      this.posts = postsData;
      this.isLoading = false
    });
  }
  
  deletePost(post){
    this.posts.splice(this.posts.indexOf(post), 1);
  }
  addPost(post){
    this.posts.push(post);
  }
  


}
